import React from "react";
import Verse from "./Verse";

const Poem = poem => (
  <div className="poem">
    <h1 className="poem-title">{poem.title}</h1>
    <div className="poem-body">{poem.lines.map(Verse)}</div>
  </div>
);

export default Poem;
