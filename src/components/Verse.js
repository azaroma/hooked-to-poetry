import React from "react";

const Verse = verse => <p>{verse}</p>;

export default Verse;
