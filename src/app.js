import React, { Component } from "react";
import { Loading, Poem } from "./components";

const url = "http://poetrydb.org";
const toJson = implementsJson => implementsJson.json();
const transformResponse = poems => ({ poems });
const incrementVisible = n => state => ({ visible: state.visible + n });

function getNPoems(n, poems) {
  if (n === 1) return [poems[0]];
  return poems.slice(0, n);
}

function logError(error) {
  console.error("The poem could not be fetched", error);
}

class Poems extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: 1,
      poems: []
    };
  }

  showMorePoems = () => this.setState(incrementVisible(5));

  componentDidMount() {
    fetch(url + "/author/Shakespeare")
      .then(toJson)
      .then(transformResponse)
      .then(poems => {
        this.setState(poems);
        return poems;
      })
      .catch(logError);
  }

  render() {
    if (this.state.poems.length === 0) return <Loading />;

    const visiblePoems = getNPoems(this.state.visible, this.state.poems);
    return (
      <div id="Poems">
        <section>{visiblePoems.map(Poem)}</section>
        <div className="more-poems-button">
          <button onClick={this.showMorePoems}>More Poems</button>
        </div>
      </div>
    );
  }
}

export default Poems;
