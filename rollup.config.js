import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import babel from "rollup-plugin-babel";

export default {
  input: "index.js",
  output: {
    dir: "dist",
    format: "iife",
    globals: {
      react: "React",
      "react-dom": "ReactDOM"
    }
  },
  cache: true,
  external: ["react", "react-dom"],
  plugins: [
    resolve({ preferBuiltins: true }),
    babel(),
    commonjs({ include: "node_modules/**" })
  ]
};
